//
//  SearchPage.swift
//  TemplateDesign
//
//  Created by TISSA Technology on 6/21/21.
//

import UIKit
import Alamofire
import SDWebImage
import SideMenu

class SearchPage: UIViewController,UISearchBarDelegate {

    @IBOutlet weak var searchbartxt: UISearchBar!
    @IBOutlet weak var menulistView: UITableView!
    
    var fetchedproduct = NSArray()
    var menulist = NSMutableArray()
    var restStatusStr = String()

    override func viewDidLoad() {
        super.viewDidLoad()

        menulistView.register(UINib(nibName: "menuCells", bundle: nil), forCellReuseIdentifier: "Cell")
        
        menulistView.rowHeight = UITableView.automaticDimension
        menulistView.estimatedRowHeight = 130
        
        ERProgressHud.sharedInstance.show(withTitle: "Loading...")
        gettiming()
        menulist = []
        searchbartxt.becomeFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        searchbartxt.becomeFirstResponder()
    }
    
    //MARK: - tab bar button actions

    @IBAction func backBtnClicked(_ sender: UIButton) {
        searchbartxt.resignFirstResponder()
        self.navigationController?.popViewController(animated: false)

    }
    
    @IBAction func moreClicked(_ sender: Any) {
        searchbartxt.resignFirstResponder()
        SideMenuManager.default.rightMenuNavigationController = storyboard?.instantiateViewController(withIdentifier: "RightMenuNavigationController") as? SideMenuNavigationController
       
        present(SideMenuManager.default.rightMenuNavigationController!, animated: true, completion: nil)
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
    
        self.fetchedproduct = []
        if searchbartxt.text == ""{
            
        }else{
            
        ERProgressHud.sharedInstance.show(withTitle: "Loading...")
        productSearchCall()
        searchBar.resignFirstResponder()
        }
        }

}

extension SearchPage {
    
    //MARK: Webservice Call Search product  by productname
    
    func productSearchCall() {
        
        let searchdata = searchbartxt.text!
        let extra = ""
        let restaurantid = GlobalClass.restaurantGlobalid
       
        let page = 0

        let urlString = GlobalClass.DevlopmentGraphql

        let stringPassed = "query{productSearch(token:\"\(GlobalClass.globGraphQlToken)\", productName :\"\(searchdata)\", restaurantId : \(restaurantid) , extra : \"\(extra)\" , first:100, skip:\(page)){\n productId\n productName\n   productUrl\n  price\n extra\n optional\n taxExempt\n category\n {\n categoryId \n category \n } \n restaurant { \n restaurantId \n address \n} } \n}"
        
        
       
        
        print("pro search w textUrl- \(stringPassed)")

            AF.request(urlString, method: .post, parameters: ["query": stringPassed],encoding: JSONEncoding.default, headers: nil).responseJSON {
           response in
             switch response.result {
             case .success:
               //  print(response)

                 if response.response?.statusCode == 200{
                
                let dict :NSDictionary = response.value! as! NSDictionary
                let status = dict.value(forKey: "data")as! NSDictionary
                print(status)
                    let productsearchArr :NSArray = status.value(forKey: "productSearch")as! NSArray
                    
                   
                
                    if productsearchArr.count == 0 {
                        
                        self.fetchedproduct = []
                        
                       
                        
                        self.showSimpleAlert(messagess:"No menu found for given search")
                       
                        self.menulistView.reloadData()
                        ERProgressHud.sharedInstance.hide()
                    }else{
                       
                        let adddishes = NSMutableArray()

                        adddishes.addObjects(from: self.fetchedproduct as! [Any])
                      
                        self.fetchedproduct = productsearchArr
                       
                        self.menulistView.reloadData()
                        ERProgressHud.sharedInstance.hide()
                    }
                  
                 }else{
                    
                    if response.response?.statusCode == 401{
                    
                        ERProgressHud.sharedInstance.hide()
                  self.SessionAlert()
                  
                    }else if response.response?.statusCode == 500{
                        
                        ERProgressHud.sharedInstance.hide()

                        let dict :NSDictionary = response.value! as! NSDictionary
                        
                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                    }else if response.response?.statusCode == 404{
                       
                        ERProgressHud.sharedInstance.hide()
                      //  self.fetchedproduct = []
                        
                        
                    }else{
                        
                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                       }
                    
                    
                    
                 }
                 
                 break
             case .failure(let error):
                
                ERProgressHud.sharedInstance.hide()

                print(error.localizedDescription)
                
                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                
                let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                
                let msgrs = "URLSessionTask failed with error: The request timed out."
                
                if error.localizedDescription == msg {
                    
            self.showSimpleAlert(messagess:"No internet connection")
                    
        }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                    
            self.showSimpleAlert(messagess:"Slow internet detected")
                    
                }else{
                
                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                }

                   print(error)
            }
           }


        }
    
}

extension SearchPage {
    

func showSimpleAlert(messagess : String) {
    let alert = UIAlertController(title: "", message: messagess,         preferredStyle: UIAlertController.Style.alert)

  
    alert.addAction(UIAlertAction(title: "OK",
                                  style: UIAlertAction.Style.default,
                                  handler: {(_: UIAlertAction!) in
                                    ERProgressHud.sharedInstance.hide()
                               
    }))
    self.present(alert, animated: true, completion: nil)
    alert.view.tintColor = UIColor.black
}


func SessionAlert() {
    let alert = UIAlertController(title: "Session Expired", message: "Please login again.",         preferredStyle: UIAlertController.Style.alert)

  
    alert.addAction(UIAlertAction(title: "OK",
                                  style: UIAlertAction.Style.default,
                                  handler: {(_: UIAlertAction!) in
                                    ERProgressHud.sharedInstance.hide()
                                    
                               //     ProgressHUD.dismiss()

                                    //Sign out action
                                  
                                    UserDefaults.standard.removeObject(forKey: "AvlbCartId")
                                    UserDefaults.standard.removeObject(forKey: "storeIdWRTCart")
                                    UserDefaults.standard.removeObject(forKey: "custToken")
                                    UserDefaults.standard.removeObject(forKey: "custId")
                                UserDefaults.standard.removeObject(forKey: "Usertype")
                                UserDefaults.standard.synchronize()
                                    
                                    let login = self.storyboard?.instantiateViewController(withIdentifier: "LoginPage") as! LoginPage
                                    self.navigationController?.pushViewController(login, animated: true)
                                    
                                    
                                   
    }))
    self.present(alert, animated: true, completion: nil)
    alert.view.tintColor = UIColor.black
}
}

//MARK: - Table View Delegates And Datasource
extension SearchPage: UITableViewDelegate,UITableViewDataSource{

func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
 
    return fetchedproduct.count
    
}

// create a cell for each table view row
func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

    let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! menuCells
    
    cell.selectionStyle = .none
   
    cell.menuinView.layer.cornerRadius = 8
    cell.menuinView.layer.borderWidth = 0.5
    cell.menuinView.layer.borderColor = UIColor(rgb: 0x5C8C49).cgColor
    cell.menuinView.layer.shadowColor =  UIColor(rgb: 0x466A37).cgColor
    cell.menuinView.layer.shadowOpacity = 1
    cell.menuinView.layer.shadowOffset = .zero
    cell.menuinView.layer.shadowRadius = 3

        cell.dishimage.layer.borderWidth = 1
        cell.dishimage.layer.masksToBounds = true
        cell.dishimage.layer.borderColor = UIColor.black.cgColor
        cell.dishimage.layer.cornerRadius = cell.dishimage.frame.width/2
        cell.dishimage.clipsToBounds = true
     

            if fetchedproduct.count == 0 {
                
            }else{
            let dictObj = self.fetchedproduct[indexPath.row] as! NSDictionary

            print(dictObj)
            
            
            var urlStr = String()
            if dictObj["productUrl"] is NSNull || dictObj["productUrl"] == nil{

                urlStr = ""

            }else{
                urlStr = dictObj["productUrl"] as! String
            }


            let rupee = "$"
            
//            let pricedata = dictObj["price"]as! Double
//            let conprice = String(pricedata)
            
            var conprice = String()
            
                if let pricedata = dictObj["price"] as? String {
                   let getprice =  Double(pricedata)
                    conprice = String(getprice!)
                }else if let pricedata = dictObj["price"] as? NSNumber {
                    let getprice =  pricedata.stringValue
                    let changetype = Double(getprice)
                   //  conprice = pricedata.stringValue
                    conprice = String(changetype!)
                }
        
            cell.dishprice.text = rupee + conprice
            
            cell.dishname.text!  = dictObj["productName"] as! String

            let url = URL(string: urlStr )


            cell.dishimage.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.dishimage.sd_setImage(with: url) { (image, error, cache, urls) in
                if (error != nil) {
                    // Failed to load image
                    cell.dishimage.image = UIImage(named: "noimage.png")
                } else {
                    // Successful in loading image
                    cell.dishimage.image = image
                }
            }

            cell.dishdiscribe.text!  = dictObj["extra"] as! String

        }
            
           

        return cell
    }
    
 
    

func tableView(_ tableView: UITableView,
               heightForRowAt indexPath: IndexPath) -> CGFloat{
   
    return UITableView.automaticDimension
 
}

func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
//    let token = UserDefaults.standard.object(forKey: "Usertype")
//    if token == nil {
//        
//        showSimpleAlert(messagess: "Please login")
//        
//    }else{

    if restStatusStr == "open" {
    
    let menu = self.storyboard?.instantiateViewController(withIdentifier: "MenuDetailPage") as! MenuDetailPage

    if menulistView.indexPathForSelectedRow != nil {
     
            let dictObj = self.fetchedproduct[indexPath.row] as! NSDictionary
            
            let reqStr = dictObj["optional"]as! Bool
            print(reqStr)
            menu.mustrequired = reqStr
           
            let pidd = dictObj["productId"]as! String

            menu.passproductid = Int(pidd)!
            menu.passproductaName = (dictObj["productName"] as? String)!

                if dictObj["productUrl"] is NSNull {
                    menu.paasprodyctimage = ""
                }else{
                    menu.paasprodyctimage = (dictObj["productUrl"] as? String)!
                }

       
            if let pricedata = dictObj["price"] as? String {
               let getprice =  Double(pricedata)
                menu.passunitprice = String(getprice!)
            }else if let pricedata = dictObj["price"] as? NSNumber {
                let getprice =  pricedata.stringValue
                let changetype = Double(getprice)
                menu.passunitprice = String(changetype!)
                
            }
        menu.dataenter = "search"
            
            let restobj = dictObj["restaurant"] as! NSDictionary
            
            let restaurantid = restobj["restaurantId"]as! String
            
            let restid = restaurantid
            let defaults = UserDefaults.standard
            
            defaults.set(restid, forKey: "clickedStoreId")
            

        
    }
    
    self.navigationController?.pushViewController(menu, animated: true)
    
}else{
    
    showSimpleAlert(messagess: "Restaurant is closed")
}

//}
}
}

extension SearchPage {
func gettiming(){
    
    var admintoken = String()
    let defaults = UserDefaults.standard
    let token = UserDefaults.standard.object(forKey: "Usertype")
    if token == nil {
        admintoken = (defaults.object(forKey: "adminToken")as? String)!
    }else{
        admintoken = (defaults.object(forKey: "custToken")as? String)!
    }
    
    let autho = "token \(admintoken)"
    
    let urlString = GlobalClass.DevlopmentApi+"hour/?restaurant_id=\(GlobalClass.restaurantGlobalid)"
    
       
    print(" categoryurl - \(urlString)")
    
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Authorization": autho
        ]
  

    AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON {
        response in
          switch response.result {
                        case .success:
                            print(response)

                            if response.response?.statusCode == 200{
                                
                                let dict :NSDictionary = response.value! as! NSDictionary
                               
                                let data  = dict["status"]as! String
                                
                                self.restStatusStr = data
                                ERProgressHud.sharedInstance.hide()

                                
                            }else{
                                
              if response.response?.statusCode == 401{
                                
                ERProgressHud.sharedInstance.hide()

                self.SessionAlert()
                      
                                
                                }
                                
                                
                                if response.response?.statusCode == 500{
                                    
                                    ERProgressHud.sharedInstance.hide()

                                    let dict :NSDictionary = response.value! as! NSDictionary
                                    
                                    self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                }
                                
                                
                                
                            }
                            
                            break
                        case .failure(let error):
                            ERProgressHud.sharedInstance.hide()

                            print(error.localizedDescription)
                            
                            let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                            
                            let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                            
                            let msgrs = "URLSessionTask failed with error: The request timed out."
                            
                            if error.localizedDescription == msg {

                        self.showSimpleAlert(messagess:"No internet connection")

                    }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{

                        self.showSimpleAlert(messagess:"Slow Internet Detected")

                            }else{
                            
                        self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                            }

                               print(error)
                            
                            
                            
                        }
        }
        
  
        
    }
}
